$(function () {

    $("#telefone").mask("(99) 9999-99999");

    $("#telefone").on("blur", function() {
    var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

    if( last.length == 3 ) {
        var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
        var lastfour = move + last;
        var first = $(this).val().substr( 0, 9 );

        $(this).val( first + '-' + lastfour );
    }
});

    $('#datapicker2').datepicker({
        format: "dd/mm/yyyy",
                    language: "pt-BR",
                    autoclose: true,
                    changeYear: true,
                    showButtonPanel: true,
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('a[data-toggle="tab"]').removeClass('btn-primary');
        $('a[data-toggle="tab"]').addClass('btn-default');
        $(this).removeClass('btn-default');
        $(this).addClass('btn-primary');
    });

    $('.next').click(function(){
        var nextId = $(this).parents('.tab-pane').next().attr("id");
        $('[href=#'+nextId+']').tab('show');
    })

    $('.prev').click(function(){
        var prevId = $(this).parents('.tab-pane').prev().attr("id");
        $('[href=#'+prevId+']').tab('show');
    })

    $('.submitWizard').click(function () {

        var nome = $("#nome").val();
        var email = $("#email").val();
        var telefone = $("#telefone").val();
        var data_nascimento = $("#datapicker2").val();
        var regiao = $("#regiao").val();
        var unidade = $("#unidade").val();

        $.post( "/send", { nome: nome, email: email, telefone: telefone, 
            data_nascimento: data_nascimento, regiao: regiao, unidade: unidade }, "json")
            .done(function( data ) {
                if (data.success == true) {
                    alert('Dados inseridos com sucesso!');
                } else {
                    alert(data.message);
                }
            });
            
    });

    $('#regiao').change(function () {

        var regiao = $("#regiao").val();
        var unidades = [];
 
        $.ajax({
            url: '/unidades/' + regiao,
            type: 'post',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (i, item) {
                    unidades.push({label: item.unidade, value: item.id});
                });
            },
        });
        $("#unidade_label").autocomplete({
            source: unidades,
            select: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
                $("#unidade").val(ui.item.value);
            }
        });       
    });

});