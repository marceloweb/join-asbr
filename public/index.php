<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

$path = explode("/",$_SERVER["REQUEST_URI"]);

$resource = empty($path[1]) ? "Main" : ucwords($path[1]);

if ($resource == 'Unidades') {
	$regiao_id = $path[2];
}

$base = $_SERVER["DOCUMENT_ROOT"];

include_once "{$base}/../src/{$resource}.php";