<?php

require_once "WsClient.php";
require_once "Process.php";

$process = new Process($_POST);
$data = $process->getData();

$result = json_encode(array("success" => false, "message" => "Dados inválidos"));

if ($data !== 0) {
	$ws = new WsClient("http://api.actualsales.com.br/join-asbr/ti/lead");
	$result = $ws->post($data);
}

header("Content-type: application/json; charset=UTF-8");
print_r($result);

