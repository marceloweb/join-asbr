
<!DOCTYPE html>
<html>
	<head>

        <meta charset="utf-8" />

		<link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css" />
		<link rel="stylesheet" href="/vendor/jquery-ui/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css" />
	    <link rel="stylesheet" href="/styles/style.css">


    </head>
    <body>    

		<div class="panel-body">
			<h1>Join ASBR</h1>
	        <form name="simpleForm" novalidate id="simpleForm" action="../src/Send.php" method="post">

				<div class="text-center m-b-md" id="wizardControl">
		            <a class="btn btn-primary" href="#step1" data-toggle="tab">Passo 1</a>
		            <a class="btn btn-default" href="#step2" data-toggle="tab">Passo 2</a>
		        </div>

		        <div class="tab-content">
		            <div id="step1" class="p-m tab-pane active">

		                <div class="row">
		                   	<div class="col-lg-3 text-center">
                                <img src="images/logo.png" class="img-thumbnail"/>
                            </div>

		                    <div class="col-lg-9">
		                        <div class="row">
		                            <div class="form-group col-lg-6">
		                                <input type="text" id="nome" class="form-control" name="nome" placeholder="Seu nome" />
		                            </div>
		                            <div class="form-group col-lg-6">
		                                <input type="text" id="datapicker2" class="form-control" name="data_nascimento" placeholder="dd/mm/dddd" />
		                            </div>
		                            <div class="form-group col-lg-6">
		                                <input type="text" id="email" class="form-control" name="email" placeholder="Seu e-mail" />
		                            </div>
		                            <div class="form-group col-lg-6">
		                                <input type="text" id="telefone" class="form-control" name="telefone" placeholder="(xx) xxxx-xxxx" />
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <div class="text-right m-t-xs">
                            <a class="btn btn-default next" href="#">Próximo Passo</a>
                        </div>

		            </div>

		            <div id="step2" class="p-m tab-pane">
		                <div class="row">

		                       <div class="col-lg-3 text-center">
                                <img src="images/logo.png" class="img-thumbnail">
                            </div>

		                    <div class="col-lg-9">
		                        <div class="row">
		                            <div class="form-group col-lg-6">
		                                <select class="form-control m-b" name="regiao" id="regiao">
                							<option>Região</option>
                							<option value="1">Centro-Oeste</option>
                							<option value="2">Nordeste</option>
                							<option value="5">Norte</option>
                							<option value="3">Sudeste</option>
                							<option value="4">Sul</option>
            							</select>
		                            </div>
		                            <div class="form-group col-lg-6">
		                                <input type="text" id="unidade_label" class="form-control" name="unidade_label" placeholder="Unidade" />
		                                <input type="hidden" id="unidade" name="unidade" />
		                            </div>
		                            
		                        </div>
		                    </div>
		                </div>

		                <div class="text-right m-t-xs">
                            <a class="btn btn-default prev" href="#">Passo Anterior</a>
		                    <a class="btn btn-success submitWizard" href="#">Salvar</a>
		                </div>

		            </div>

		        </div>
	    	</form>

	    </div>

		<script src="/vendor/jquery/dist/jquery.min.js"></script>
		<script src="/vendor/jquery-ui/jquery-ui.js"></script>
		<script src="/vendor/jquery-mask-plugin/jquery.mask.min.js"></script>
		<script src="/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
		<script src="/vendor/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.pt-BR.min.js"></script>

		
		<!-- App scripts -->
		<script src="/scripts/main.js"></script>

    </body>
</html>