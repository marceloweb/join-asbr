<?php

require_once "Connection.php";

$pdo = new Connection();
$stmt = $pdo->query("select * from unidade where regiao_id={$regiao_id}");

$result = array();

while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $result[] = array('id' => $row['unidade_id'], 'unidade' => utf8_encode($row['nome']));
}

header("Content-type: text/json; charset=UTF-8");
$json = json_encode($result);
print($json);