<?php

require_once 'Connection.php';

class Process {

	private $data;

	public function __construct($post) {
		$score = 10;
		$error = 0;
		$data = "token=cf938218cd042f9ea6b2638a46b8c783";

		foreach ($post as $key => $value) {
			if (empty(trim($value))) {
				$this->data = 0;
				$error = 1;
				break;
			}

			if ($key == "regiao") {
				$value = $this->formatRegiao($value);
			}

			if ($key == "unidade") {
				$value = $this->formatUnidade($value);
			}

			if ($key == "data_nascimento") {
				$value = $this->formatDate($value);
			}

			$score = $this->score($key, $value, $score);
			$data .= "&".$key."=".htmlspecialchars($value);
		}

		if ($error === 0) {
			$data .= "&score=" . $score;
			$result = $this->insert($post,$score);
			if ($result === false) {
				$this->data = 0;
				$error = 1;
			}
		}

		if ($error === 0) {
			$this->data = $data;
		}
	}

	public function insert($post,$score) {
		$pdo = new Connection();
		$stmt = $pdo->prepare("insert into lead values(:lead_id,:nome,:email,:telefone,:unidade_id,:data_nascimento,:score)");
		$result = $stmt->execute(array(
			':lead_id' => null,
			':nome' => $post['nome'],
			':email' => $post['email'],
			':telefone' => $post['telefone'],
			':unidade_id' => $post['unidade'],
			':data_nascimento' => $this->formatDate($post['data_nascimento']),
			':score' => $score
			));

		return $result;

	}

	public function getData() {
		return $this->data;
	}

	public function formatRegiao($regiao) {
		
		switch ($regiao) {
			case 1:
				$value = "Centro-Oeste";
				break;
			
			case 2:
				$value = "Nordeste";
				break;

			case 3:
				$value = "Sudeste";
				break;
				
			case 4:
				$value = "Sul";
				break;		
		}	

		return $value;
	}

	public function formatUnidade($unidade) {
		switch ($unidade) {
			case 1:
				$value = "Brasília";
				break;
			
			case 2:
				$value = "Recife";
				break;

			case 3:
				$value = "Salvador";
				break;
				
			case 4:
				$value = "Belo Horizonte";
				break;
				
			case 5:
				$value = "Rio de Janeiro";
				break;
				
			case 6:
				$value = "São Paulo";
				break;			

			case 7:
				$value = "Curitiba";
				break;

			case 8:
				$value = "Porto Alegre";
				break;	
		}

		return $value;
	}

	public function formatDate($date) {
		$data = explode("/", $date);
		$newDate = $data[2] . "-" . $data[1] . "-" . $data[0];

		return $newDate;
	}

	public function score($key, $value, $score) {

		if ($key == 'unidade') {
			switch ($value) {
				case 'Brasília':
					$score = $score - 3;
					break;

				case 'Salvador':
				case 'Recife':
					$score = $score - 4;
					break;
					
				case 'Belo Horizonte':
				case 'Rio de Janeiro':
					$score = $score - 1;
					break;

				case 'Curitiba':
				case 'Porto Alegre':
					$score = $score - 2;
					break;
			}
		}

		if ($key == 'data_nascimento') {
			$age = $this->age($value);
			if ($age > 100 || $age < 18) {
				$score = $score - 5;
			}

			if ($age >= 40 && $age <= 99) {
				$score = $score - 3;
			}
		}

		return $score;
	}

	public function age($data_nascimento) {

		list($year,$day,$month) = explode("-", $data_nascimento);

		$birthday = mktime(0, 0, 0, $day, $month, $year); 
		$today = mktime(0, 0, 0, 06, 01, 2016); 

		$age = floor((((($today - $birthday) / 60) / 60) / 24) / 365.25);

		return $age;
	}

}