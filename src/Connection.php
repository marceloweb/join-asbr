<?php

class Connection extends PDO
{
    public function __construct()
    {
    	$config = include '../config/app.config.php';

        $dns = "mysql:host={$config['host']};dbname=asbr";
        parent::__construct($dns, $config['user'], $config['passwd']);
    }
}