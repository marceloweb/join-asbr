<?php

class WsClient {

	private $url;

	public function __construct($url) {
		$this->url = $url;
	}

	public function post($data) {
		$process = curl_init($this->url); 
		curl_setopt($process, CURLOPT_TIMEOUT, 30);  
		curl_setopt($process, CURLOPT_POSTFIELDS, $data); 
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($process, CURLOPT_POST, 1); 
		$return = curl_exec($process); 
		curl_close($process); 
		return $return; 
	}
}
